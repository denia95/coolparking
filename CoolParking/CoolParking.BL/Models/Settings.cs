﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static int InitialBalanceOfParking = 0;
        public static int ParkingCapacity = 10;
        public static int PaymentWriteOffPeriodNSeconds = 5;
        public static int LoggingPeriodNSeconds = 60;
        public static decimal Penalty = 2.5m;
        public static Dictionary<VehicleType, decimal> Rate = new Dictionary<VehicleType, decimal>
        {
            { VehicleType.PassengerCar, 2 },
            { VehicleType.Truck, 5 },
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1 }
        };
    }
}


