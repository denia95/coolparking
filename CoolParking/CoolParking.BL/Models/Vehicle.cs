﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; private set; }
        public decimal Balance { get; internal set; }
        public VehicleType VehicleType { get; private set; }

        public Vehicle(string id, VehicleType vehicletype, decimal balance)
        {
            Validate(id, balance);

            Id = id;
            Balance = balance;
            VehicleType = vehicletype;
        }

        private void Validate(string id, decimal balance)
        {
            if (id.Length != 10)
            {
                throw new ArgumentException("Wrong Id Format");
            }

            CheckLetter(id[0]);
            CheckLetter(id[1]);
            CheckLetter(id[8]);
            CheckLetter(id[9]);

            CheckSymbol(id[2]);
            CheckSymbol(id[7]);

            CheckNumber(id[3]);
            CheckNumber(id[4]);
            CheckNumber(id[5]);
            CheckNumber(id[6]);

            if (balance < 0)
            {
                throw new ArgumentException("Balance should not be negative");
            }
        }

        public Vehicle(VehicleType vehicletype, decimal balance)
        {
            Id = GenerateRandomRegistrationPlateNumber();
            Balance = balance;
            VehicleType = vehicletype;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string res = string.Empty;
            Random rnd = new Random();

            int a = rnd.Next(65, 90);
            res += (char)a;
            a = rnd.Next(65, 90);
            res += (char)a;

            res += '-';

            a = rnd.Next(0, 9);
            res += a;
            a = rnd.Next(0, 9);
            res += a;
            a = rnd.Next(0, 9);
            res += a;
            a = rnd.Next(0, 9);
            res += a;

            res += '-';

            a = rnd.Next(65, 90);
            res += (char)a;
            a = rnd.Next(65, 90);
            res += (char)a;

            return res;
        }

        private void CheckLetter(char c)
        {
            if ('A' <= c && c >= 'Z')
            {
                throw new ArgumentException("Wrong Id Format");
            }
        }

        private void CheckSymbol(char c)
        {
            if (c != '-')
            {
                throw new ArgumentException("Wrong Id Format");
            }
        }

        private void CheckNumber(char c)
        {
            if ('0' <= c && c >= '9')
            {
                throw new ArgumentException("Wrong Id Format");
            }
        }
    }
}
