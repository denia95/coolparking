﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    
    public class ParkingService : IParkingService
    {
        private Parking _parking;
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        public List<TransactionInfo> Transactions { get; set; }
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetInstance();
            
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += OnWithdrawTimedEvent;
            _withdrawTimer.Interval = Settings.PaymentWriteOffPeriodNSeconds*1000;

            _logTimer = logTimer;
            _logTimer.Elapsed += OnLogTimedEvent;
            _logTimer.Interval = Settings.LoggingPeriodNSeconds*1000;

            _logService = logService;

            Transactions = new List<TransactionInfo>();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            ValidateAddVehicle(vehicle);
            _parking.Vehicles.Add(vehicle);

            if (_parking.Vehicles.Count == 1)
            {
                _logTimer.Start();
                _withdrawTimer.Start();
            }
        }

        public void Dispose()
        {
            _parking.Vehicles.Clear();
            _parking.Balance = 0;
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle> (_parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            ValidateRemoveVehicle(vehicleId);

            foreach (var item in _parking.Vehicles)
            {
                if(item.Id == vehicleId)
                {
                    _parking.Vehicles.Remove(item);
                    break;
                }
            }

            if (_parking.Vehicles.Count == 0)
            {
                _logTimer.Stop();
                _withdrawTimer.Stop();
            }
        }

        private void ValidateRemoveVehicle(string vehicleId)
        {
            if (!_parking.Vehicles.Any(x => x.Id == vehicleId))
            {
                throw new ArgumentException($"There are not exist vehicle with Id {vehicleId}");
            }

            if (_parking.Vehicles.Any(x => x.Id == vehicleId && x.Balance < 0))
            {
                throw new InvalidOperationException("You cant remove vehicle from Parking with negative balance");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            ValidateTopUpVehicle(vehicleId, sum);

            foreach (var item in _parking.Vehicles)
            {
                if(item.Id == vehicleId)
                {
                    item.Balance += sum;
                    return;
                }
            }
        }

        public int GetCapacity()
        {
            return GetFreePlaces();
        }

        public decimal GetSumFromLastTransaction()
        {
            return Transactions.Sum(x => x.Sum);
        }

        private void ValidateAddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Count == Settings.ParkingCapacity)
            {
                throw new InvalidOperationException();
            }

            if (_parking.Vehicles.Any(x => x.Id == vehicle.Id))
            {
                throw new ArgumentException();
            }
        }

        private void ValidateTopUpVehicle(string vehicleId, decimal sum)
        {
            if (!_parking.Vehicles.Any(x => x.Id == vehicleId))
            {
                throw new ArgumentException($"There are not exist vehicle with Id {vehicleId}");
            }

            if (sum < 0)
            {
                throw new ArgumentException("sum cant be negative");
            }
        }

        public void OnWithdrawTimedEvent(object o, ElapsedEventArgs e)
        {
            foreach (var item in _parking.Vehicles)
            {
                decimal rate;

                if (item.Balance >= Settings.Rate[item.VehicleType])
                {
                    rate = Settings.Rate[item.VehicleType];
                    item.Balance -= rate;
                    _parking.Balance += rate;
                }
                else if(item.Balance > 0)
                {
                    rate = Settings.Penalty * (Settings.Rate[item.VehicleType] - item.Balance);
                    _parking.Balance += item.Balance + rate;
                    item.Balance -= rate;
                }
                else
                {
                    rate = Settings.Penalty * Settings.Rate[item.VehicleType];
                    _parking.Balance += rate;
                    item.Balance -= rate;
                }

                WriteTransaction(DateTime.Now, item.Id, rate);
            }
        }

        public void OnLogTimedEvent(object o, ElapsedEventArgs e)
        {
            string logInfo = GetLastTransactions(Transactions);
            _logService.Write(logInfo);

            Transactions.Clear();
        }

        public void WriteTransaction(DateTime time, string id, decimal rate)
        {
            TransactionInfo transactionInfo = new TransactionInfo
            {
                TransactionTime = time,
                VehicleId = id,
                Sum = rate
            };

            Transactions.Add(transactionInfo);
        }

        public string GetLastTransactions(List<TransactionInfo> transactions)
        {
            string logInfo = string.Empty;
            for (int i = 0; i < transactions.Count; i++)
            {
                logInfo += $"{transactions[0].Sum} was withdraw from {transactions[i].VehicleId} on {transactions[i].TransactionTime} \n";
            }

            return logInfo;
        }
    }
}