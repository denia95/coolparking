﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;


namespace CoolParking
{
    public class Program
    {
        static void Main(string[] args)
        {
            string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            ITimerService _withdrawTimer = new TimerService();
            ITimerService _logTimer = new TimerService();
            ILogService _logService = new LogService(_logFilePath);
            ParkingService _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);

            
            ConsoleKeyInfo keyinfo;
            Console.WriteLine("Hello Neo! You are in my Parking Program.");
            do
            {
                Console.WriteLine();
                Console.WriteLine("Choose the options below you want to trigger:");
                Console.WriteLine("1 - Get current Balance of Parking");
                Console.WriteLine("2 - Get current transaction balance");
                Console.WriteLine("3 - Get free places");
                Console.WriteLine("4 - Show last parking transactions");
                Console.WriteLine("5 - Show history of all transactions");
                Console.WriteLine("6 - Show all vehicles on parking");
                Console.WriteLine("7 - Add Vehicle to Parking");
                Console.WriteLine("8 - Remove Vehicle from Parking");
                Console.WriteLine("9 - Top Up the vehicle");
                Console.WriteLine("0 - Exit from Parking Program");
                Console.WriteLine();

                keyinfo = Console.ReadKey();
                try
                {
                    switch (keyinfo.Key)
                    {
                        case ConsoleKey.D1:
                            Console.WriteLine("Current balance of parking is: " + _parkingService.GetBalance()); Console.WriteLine();
                            break;
                        case ConsoleKey.D2:
                            Console.WriteLine("Current transaction balance is: " + _parkingService.GetSumFromLastTransaction()); Console.WriteLine();
                            break;
                        case ConsoleKey.D3:
                            Console.WriteLine($"Parking has {_parkingService.GetFreePlaces()} free places."); Console.WriteLine();
                            break;
                        case ConsoleKey.D4:
                            GetLastTransactions(_parkingService.GetLastParkingTransactions());
                            Console.WriteLine();
                            break;
                        case ConsoleKey.D5:
                            Console.WriteLine(_parkingService.ReadFromLog()); Console.WriteLine();
                            break;
                        case ConsoleKey.D6:
                            ShowVehicles(_parkingService.GetVehicles());
                            Console.WriteLine();
                            break;
                        case ConsoleKey.D7:
                            _parkingService.AddVehicle(AddVehicle());
                            Console.WriteLine("Vehicle was added successfully"); Console.WriteLine();
                            break;
                        case ConsoleKey.D8:
                            _parkingService.RemoveVehicle(RemoveVehicle());
                            Console.WriteLine("Vehicle was removed successfully"); Console.WriteLine();
                            break;
                        case ConsoleKey.D9:
                            _parkingService.TopUpVehicle(GetId(), GetSum());
                            Console.WriteLine("Balance was increased successfully"); Console.WriteLine();
                            break;
                        default:
                            Console.WriteLine("Please choose number from 0-9");
                            break;
                    }
                }
                catch (ArgumentException e) { Console.WriteLine(e.Message); }
                catch (InvalidOperationException e) { Console.WriteLine(e.Message); }
                catch (Exception e) { }
            }
            while (keyinfo.Key != ConsoleKey.D0);
        }

        private static void GetLastTransactions(TransactionInfo[] transactions)
        {
            foreach (var item in transactions)
            {
                Console.WriteLine($"{item.Sum} was withdraw from {item.VehicleId} on {item.TransactionTime} \n");
            }
        }

        public static Vehicle AddVehicle()
        {
            Vehicle res;
            try
            {
                string id = GetId();
                Console.WriteLine("Input Type of Vehicle: ");
                Console.WriteLine("Possible types are: PassengerCar, Truck, Bus, Motorcycle");
                var type = Console.ReadLine();
                Console.Write("Input Balance of Vehicle: ");
                var balance = Convert.ToDecimal(Console.ReadLine());

                VehicleType vType = VehicleType.PassengerCar;
                switch (type)
                {
                    case "PassengerCar":
                        vType = VehicleType.PassengerCar;
                        break;
                    case "Truck":
                        vType = VehicleType.Truck;
                        break;
                    case "Bus":
                        vType = VehicleType.Bus;
                        break;
                    case "Motorcycle":
                        vType = VehicleType.Motorcycle;
                        break;
                    default:
                        Console.WriteLine(type + " is not mentioned above. Please input vehicle details again.");
                        break;
                }

                res = new Vehicle(id, vType, balance);
                return res;
            }
            catch (ArgumentException ae)
            {
                Console.WriteLine(ae.Message);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine("Balance is not correct. Please input vehicle details again.");
                throw;
            }

            return null;
        }

        public static string RemoveVehicle()
        {
            return GetId();
        }

        private static string GetId()
        {
            Console.Write("Input Id of Vehicle: ");
            return Console.ReadLine();
        }

        private static decimal GetSum()
        {
            Console.Write("Input Sum of Vehicle: ");
            return Convert.ToDecimal(Console.ReadLine());
        }

        private static void ShowVehicles(ReadOnlyCollection<Vehicle> vehicles)
        {
            foreach(var item in vehicles)
            {
                Console.WriteLine($"Vehicle {item.Id} with balance {item.Balance}");
            }
        }
    }
}
